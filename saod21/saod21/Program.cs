﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace saod21
{
    class Program
    {
        static void Main(string[] args)
        {

            Stack obj1 = new Stack();
            obj1.ShowList();
            obj1.AddValue(1);
            obj1.Insert(2, 5);
            obj1.ShowList();
            obj1.Insert(3, 5);
            obj1.ShowList();
            obj1.AddValue(3);
            obj1.ShowList();
            obj1.First();
            obj1.Last();
            obj1.At(2);
            obj1.Clear();
            obj1.Empty();




            Console.Read();
        }
        class Stack
        {
            private int n;
            private int[] M;

            public Stack()
            {
                M = new int[0];
                n = 0;
            }
            public void Insert(int r, int x)
            {

                Array.Resize(ref M, n + 1);

                for (int i = n; i > r + 1; i--)
                {
                    M[i] = M[i - 1];
                    M[r] = x;
                }
                n++;
            }
            public void ShowList()
            {
                for (int i = 0; i < n; i++)
                {
                    Console.Write(M[i]);
                    Console.Write(" ");
                }
                Console.WriteLine(" ");
            }
            public void AddValue(int x)
            {
                Array.Resize(ref M, n + 1);

                M[n] = x;
                n++;
            }
            public void RemoveAt(int r, int x)
            {

                Array.Resize(ref M, n - 1);
                n--;
                for (int i = r; i < n; i++)
                {
                    M[i] = M[i + 1];
                }

            }
            public void First()
            {
                if (n > 0)
                {
                    Console.Write(M[0]);
                }

            }
            public void Last()
            {
                if (n > 0)
                {
                    Console.Write(M[n - 1]);
                }

            }
            public void Clear()
            {
                n = 0;
                Array.Resize(ref M, 0);
            }
            public void At(int r)
            {
                if (n > r)
                {
                    Console.Write(M[r]);
                }
            }
            public void Empty()
            {
                if (n == 0)
                {
                    Console.Write(" true");
                }
                else
                    Console.Write("false ");
            }

        }
    }
}
